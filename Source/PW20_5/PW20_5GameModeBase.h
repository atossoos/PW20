// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PW20_5GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PW20_5_API APW20_5GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
