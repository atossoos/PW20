// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Bonus.generated.h"

class UStaticMeshComponent;

UCLASS()
class MYPROJECT4_API ABonus : public AActor, public IInteractable
{
	GENERATED_BODY()
	

public:	
	// Sets default values for this actor's properties
	ABonus();

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UStaticMeshComponent* BonusSpawn;
	TSubclassOf<ABonus> ABonusClass;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	void CreateBonous();
	void BonusSpawner();
};
