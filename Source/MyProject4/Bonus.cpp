#include "Bonus.h"
// Fill out your copyright notice in the Description page of Project Settings.
#include "Bonus.h"
#include "SnakeBase.h"

// Sets default values
ABonus::ABonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ABonus::CreateBonous()
{
	BonusSpawn = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	BonusSpawn->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ABonus::BonusSpawner()
{
	FVector NewLocation(FMath::RandRange(-400,400), FMath::RandRange(-400,400), 4);
	FTransform NewTransform(NewLocation);
	GetWorld()->SpawnActor<ABonus>(ABonusClass, NewTransform);
}

// Called when the game starts or when spawned
void ABonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonus::Interact(AActor* Interactor, bool bIsHead)
{

	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			BonusSpawner();
			Snake->SetActorTickInterval(0.f);
			Destroy();
		}
	}
}
